﻿namespace JIS.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Marked_Field_Required : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ApplicationModels", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.ApplicationModels", "MobileNo", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.ApplicationModels", "Courses", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ApplicationModels", "Courses", c => c.String());
            AlterColumn("dbo.ApplicationModels", "MobileNo", c => c.String());
            AlterColumn("dbo.ApplicationModels", "Name", c => c.String());
        }
    }
}
