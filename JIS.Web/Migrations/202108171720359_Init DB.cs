﻿namespace JIS.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MobileNo = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Address = c.String(),
                        CounsellingDate = c.DateTime(nullable: false),
                        Courses = c.String(),
                        RegDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ApplicationModels");
        }
    }
}
