﻿namespace JIS.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Id_DateType_Change_From_Int_to_Long : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ApplicationModels");
            AlterColumn("dbo.ApplicationModels", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.ApplicationModels", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ApplicationModels");
            AlterColumn("dbo.ApplicationModels", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ApplicationModels", "Id");
        }
    }
}
