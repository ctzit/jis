﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JIS.Web.Models
{
    public class RegistrationRepository
    {
        public static bool SaveRegistration(ApplicationModel application, out string  Message)
        {
            Message = string.Empty;
            DBContextJIS db = new DBContextJIS();

            if(db.Applications.Any(x=>x.MobileNo.Equals(application.MobileNo,StringComparison.InvariantCultureIgnoreCase)))
            {
                Message = "You are already registred for the counselling.";
                return false;
            }
            application.RegDate = DateTime.UtcNow;
            //db.Applications.Add(application);
            db.Entry<ApplicationModel>(application).State = System.Data.Entity.EntityState.Added;
            try
            {
                if (db.SaveChanges() > 0)
                {
                   
                    return true;
                }
            }
            catch (Exception ex)
            {

                Message = ex.Message;
            }

            return false;

        }
    }
}