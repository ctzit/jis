﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JIS.Web.Models
{
    using System.Web.Mvc;

    public  class  DateOfBirthHelper
    {
        public static List<SelectListItem> DD()
        {
            List<SelectListItem> DD = new List<SelectListItem>();
            for (int i = 1; i <= 31; i++)
            {
                DD.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            DD.Insert(0, new SelectListItem { Value = "-1", Text = "--Select Day--" });
            return DD;
        }
        public static List<SelectListItem> MM()
        {
            List<SelectListItem> MM = new List<SelectListItem>();

            for (int i = 1; i <= 12; i++)
            {
                MM.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            MM.Insert(0, new SelectListItem { Value = "-1", Text = "--Select Month--" });

            return MM;
        }
        public static List<SelectListItem> YYYY()
        {
            List<SelectListItem> YYYY = new List<SelectListItem>();
            int Startyear = (DateTime.Now.Year - 14);
            int Endyear = Startyear - 25;
            for (int i = Endyear; i <= Startyear; i++)
            {
                YYYY.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            YYYY.Insert(0, new SelectListItem { Value = "-1", Text = "--Select Year--" });

            return YYYY;
        }

        public static List<SelectListItem> CounsellingDate()
        {
            List<DateTime> counsellingDatesList = new List<DateTime>();
            //counsellingDatesList.Add(new DateTime(2021,08,21));
            //counsellingDatesList.Add(new DateTime(2021,08,28));
            counsellingDatesList.Add(new DateTime(2021,09,04));
            
            List<SelectListItem> counsellingDateSelectListItem = new List<SelectListItem>();

            foreach (var item in counsellingDatesList)
            {
                counsellingDateSelectListItem.Add(new SelectListItem {Value = item.ToString("MM-dd-yyyy"), Text=item.ToString("dd-MM-yyyy"), Selected=true });
            }

            return counsellingDateSelectListItem;
        }
        public static List<SelectListItem> Courses()
        {
            string courseStr = @"
B.Tech
Polytechnic
BBA
BCA
MBA
B.Sc.-Agriculture
B.Tech-Biotech 
B.Tech-Bio-Medical
MCA
BHM
Hospital Management
Hotel Management
D.Pharma
B.Pharma
LLB
BBA-LLB
BDS
DMLT
BMLT
M.Tech
";
            List<string> Courses = new List<string>();
            Courses.AddRange(courseStr.Split(new string[]  {"\r\n"},StringSplitOptions.RemoveEmptyEntries).ToList());


            List<SelectListItem> CourseSelectListItemList = new List<SelectListItem>();

            foreach (var item in Courses)
            {
                CourseSelectListItemList.Add(new SelectListItem { Value = item.ToString(), Text = item.ToString() });
            }

            return CourseSelectListItemList;

        }
    }
}
