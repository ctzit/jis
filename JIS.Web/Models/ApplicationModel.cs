﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JIS.Web.Models
{
    public class ApplicationModel
    {
        [Key]
        public long Id { get; set; }
        [Display(Name = "Candidate Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Mobile No.")]
        [StringLength(10)]
        [Required]
        public string MobileNo { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Full Address")]
        [DataType(DataType.MultilineText)]

        public string Address { get; set; }

        [Display(Name = "Counselling Date")]
        [Required]
        public DateTime CounsellingDate { get; set; }

        [Display(Name = "Select Course")]
        [Required]
        public string Courses { get; set; }

        [Display(Name = "Registration Date")]
        public DateTime RegDate { get; set; }
    }
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
   
}