﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.IO;
using System.Web;
using System.Web.UI;


namespace JIS.Web.Models
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;



    public class CounsellingLetterHelper
    {

        public static MemoryStream getCounselling(string Name, DateTime DOB, long CandidateId, string MobileNo, string Course,  DateTime CounsellingDate)
        {

            string reader = string.Empty;
            reader = GetHtmlTemplateOfCounselling();
            string s = reader.Replace("#MOBILENO#", MobileNo).Replace("#COUNSELLINGDATE#", string.Format("{0:dd/MMM/yyyy}", CounsellingDate)).Replace("#Name#", Name).Replace("#ApplicationNo#", CandidateId.ToString("D7")).Replace("#DOB#", DOB.ToString("dd/MM/yyyy")).Replace("#Course#", Course.Replace(",", " ,"));
            return CovertToPDF(s);
        }
        private static string GetHtmlTemplateOfCounselling()
        {
            string counsellingFormat = string.Empty;

            counsellingFormat = NetworkHelper.CallApiPoint(new ApiEndPoint().CounsellingLetterBaseUrl + "CL_WB_2021.html");
            return counsellingFormat;
        }
        public static MemoryStream CovertToPDF(string HTMLRawString)
        {
            if (string.IsNullOrEmpty(HTMLRawString)) return null;
            string FontPath = string.Empty;
            //FontPath = HttpRuntime.AppDomainAppPath + "\\Content\\fonts\\";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            StringReader sr = new StringReader(HTMLRawString);
            //Document pdfDoc = new Document(PageSize.A4,
            Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            MemoryStream stream = new MemoryStream();
            PdfWriter.GetInstance(pdfDoc, stream);
            pdfDoc.AddHeader("Header", "Counselling Letter");
            pdfDoc.Open();
            // step 4.1: register a unicode font and assign it an alliasF:\Working Project\JKSS\JKSS New\CCB2016\MissionAdmission\bootstrap\fonts\Nirmala.ttf
            //FontFactory.Register(FontPath + "kruti-dev-010.ttf", "nirmala");
            //FontFactory.Register(FontPath + "BijoyRegular.ttf", "vrinda");
            //FontFactory.Register(FontPath + "Nirmala.ttf", "nirmala");
            // step 4.2: create a style sheet and set the encoding to Identity-H
            iTextSharp.text.html.simpleparser.StyleSheet ST = new iTextSharp.text.html.simpleparser.StyleSheet();
            ST.LoadTagStyle("body", "encoding", "Identity-H");
            // step 4.3: assign the style sheet to the html parser
            htmlparser.SetStyleSheet(ST);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            return stream;
        }
    }


}