﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace JIS.Web.Models
{
    public partial class DBContextJIS:DbContext
    {
        public DbSet<ApplicationModel> Applications { get; set; }

    }

    public partial class DBContextJIS : DbContext
    {
        public DBContextJIS():base("DefaultConnection")
           
        {

        }

        public static DBContextJIS Create()
        {
            return new DBContextJIS();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DBContextJIS>(null);
            base.OnModelCreating(modelBuilder);



        }

        public override int SaveChanges()
        {

            try
            {
                this.Configuration.ValidateOnSaveEnabled = false;
                //  context.Configuration.ValidateOnSaveEnabled = false;
                return base.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // saveFailed = true;

                // Get the current entity values and the values in the database 
                var entry = ex.Entries.Single();
                var currentValues = entry.CurrentValues;
                var databaseValues = entry.GetDatabaseValues();

                // Choose an initial set of resolved values. In this case we 
                // make the default be the values currently in the database. 
                var resolvedValues = databaseValues.Clone();

                // Have the user choose what the resolved values should be 
                //  HaveUserResolveConcurrency(currentValues, databaseValues, resolvedValues);

                // Update the original values with the database values and 
                // the current values with whatever the user choose. 
                entry.OriginalValues.SetValues(databaseValues);
                entry.CurrentValues.SetValues(resolvedValues);
                return 0;
            }
            catch (DbEntityValidationException e)
            {


                foreach (var eve in e.EntityValidationErrors)
                {
                    Debug.WriteLine(
                        "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name,
                        eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw e;
            }
        }

    }
}