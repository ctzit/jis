﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JIS.Web.Models

{
    public class ApiEndPoint
    {
        string UrlScheme = string.Empty;
        string AppBasePath = string.Empty;

        private string CounsellingBaseUrl;
        public string CounsellingLetterofCandidateBaseUrl { get; set; }
        private string ControlPanelUrl { get; set; }
        private string BaseUrl;
        private string BaseTemplateUrl;
        public string WhatsAppApiKey;
        public string StatesApiURL { get; set; }
        public string SMSBalanceAPI { get; set; }
        public ApiEndPoint()
        {
            var context = HttpContext.Current.Request;

            AppBasePath = string.Format("{0}://{1}{2}/", context.Url.Scheme, context.Url.Host, (context.Url.Port == 80 ? "" : ":" + context.Url.Port.ToString()));
            UrlScheme = context.Url.Scheme;

            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Instance"]))
            {
                string loInstance = ConfigurationManager.AppSettings["Instance"].ToString();
                if (loInstance.Equals("Development", StringComparison.InvariantCultureIgnoreCase))
                {
                    SetDevelopmentAPI();
                }
                if (loInstance.Equals("Production", StringComparison.InvariantCultureIgnoreCase))
                {
                    SetProductionAPI();
                }
            }
            else
            {
                SetProductionAPI();
            }
        }

        private void SetDevelopmentAPI()
        {

            ControlPanelUrl = UrlScheme + "://localhost:57758/";
            CounsellingBaseUrl = AppBasePath +"api/CounsellingSeatAllotment/Allot?Id=";
            CounsellingLetterofCandidateBaseUrl = AppBasePath + "c/";
            BaseUrl = AppBasePath + "api/";
            BaseTemplateUrl = AppBasePath + "Templets/";

            StatesApiURL = UrlScheme + "://ccbnic.in/UMBRACO/API/State/GET/";
            SMSBalanceAPI = "http://sms.goldpi.com/api/balance.php?user=101559&key=010002Bd30l5auTMjeZQ";

        }

        private void SetProductionAPI()
        {
            ControlPanelUrl = "http://jis.ccbnic.in/";
            BaseUrl = AppBasePath + "api/";
            BaseTemplateUrl = AppBasePath + "Templets/";
            CounsellingLetterofCandidateBaseUrl = AppBasePath + "c/";
            CounsellingBaseUrl = AppBasePath + "api/CounsellingSeatAllotment/Allot?Id=";


            StatesApiURL = UrlScheme + "://ccbnic.in/UMBRACO/API/State/GET/";
            SMSBalanceAPI = "http://sms.goldpi.com/api/balance.php?user=101559&key=010002Bd30l5auTMjeZQ";

        }


        //======================================Registration-Payment Status END=========================================

        private static string _newRegistration = "CCBRistration/NewRegistration/";
        private static string _checkApplication = "CheckApplication/";
        private static string _getApplication = "GetApplication/";
        private static string _updatePaymentStatus = "UpdatePaymentStatus/";
        public string NewRegistration
        {
            get
            {
                return BaseUrl + _newRegistration;
            }
            set
            {
                NewRegistration = value;
            }
        }
        public string CheckApplication
        {
            get
            {
                return BaseUrl + _checkApplication;
            }
            set
            {
                CheckApplication = value;
            }
        }
        public string GetApplication
        {
            get
            {
                return BaseUrl + _getApplication;
            }
            set
            {
                GetApplication = value;
            }
        }
        public string UpdatePaymentStatus
        {
            get
            {
                return BaseUrl + _updatePaymentStatus;
            }
            set
            {
                UpdatePaymentStatus = value;
            }
        }
        //==============================Counselling Letter, Application Receipt==========
        private static string _ApplicationReceipt = "Aknowledgement/";
        private static string _CounsellingLetter = "CounsellingLetter/";
        private static string _AllotmentLetter = "Allotment/";
        private static string _AdvanceReceiptLetterBaseUrl = "AdvancePayment/";

        public string CounsellingLetterBaseUrl
        {
            get
            {
                return BaseTemplateUrl + _CounsellingLetter;
            }
            set
            {
                CounsellingLetterBaseUrl = value;
            }
        }

        public string ApplicationReceiptLetterBaseUrl
        {
            get
            {
                return BaseTemplateUrl + _ApplicationReceipt;
            }
            set
            {
                ApplicationReceiptLetterBaseUrl = value;
            }
        }

        public string AllotmentLetterBaseUrl
        {
            get
            {
                return BaseTemplateUrl + _AllotmentLetter;
            }
            set
            {
                AllotmentLetterBaseUrl = value;
            }
        }
        public string AdvanceReceiptLetterBaseUrl
        {
            get
            {
                return BaseTemplateUrl + _AdvanceReceiptLetterBaseUrl;
            }
            set
            {
                AdvanceReceiptLetterBaseUrl = value;
            }
        }

        //API END

        public string GetCounsellingDateAPIURL(long ApplicationId)
        {

            return CounsellingBaseUrl + ApplicationId.ToString();
        }


    }

    public class ApiCallHelper : ApiEndPoint
    {
        private static readonly HttpClient _httpClient = new HttpClient();
        public static string GetReleases(string url)
        {

            var response = _httpClient.GetStringAsync(new Uri(url)).Result;

            return response;

        }
    }


}