namespace JIS.Web.Models
{
    using System;

    public class ServiceServerNotFoundException : Exception
    {
        public ServiceServerNotFoundException() : base()
        {

        }

        public ServiceServerNotFoundException(string message) : base(message)
        {

        }
        public ServiceServerNotFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}