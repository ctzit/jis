﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
namespace JIS.Web.Models
{



    public static class NetworkHelper
    {
        public static string CallApiPoint(string url)
        {
            try
            {
                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return responseString;
            }
           
            catch (Exception ex)
            {
               
                if (ex.Message.Contains("The remote name could not be resolved") || ex.Message.Contains("The operation has timed out"))
                {
                    throw new ServiceServerNotFoundException("Service server is down, please try after some time.");
                }
                throw ex;
            }

        }

       
    }
}
