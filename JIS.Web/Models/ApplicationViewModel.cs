﻿using System;

namespace JIS.Web.Models
{
    public class ApplicationViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public DateTime? CounsellingDate { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string State { get; set; }
        public string SelectedCourse { get; set; }

        //For UI Only
        public int DD { get; set; }
        public int MM { get; set; }
        public int YYYY { get; set; }


        public DateTime? GetDOB()
        {
            if (this.YYYY > 0 && this.MM > 0 && this.DD > 0)
            {
                DateTime _Date = new DateTime(this.YYYY, this.MM, this.DD).Date;
                return _Date;
            }
            return null;
        }
    }

}