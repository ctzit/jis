﻿using JIS.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace JIS.Web.Controllers
{
    public class DownloadController : Controller
    {
        DBContextJIS db = new DBContextJIS();

        // GET: Download
        public ActionResult Index()
        {
            //return RedirectToAction(nameof(LetterAsync), new { Id = 45 });
            return View();
        }
        public ActionResult CounsellingLetter()
        {
            LoadUI();
            return View();

        }
        [HttpPost]
        public ActionResult CounsellingLetter(ApplicationViewModel applicant)
        {
            bool IsRegistredCandidate = false;
            ApplicationModel candidateApplication = null;
            var dob = applicant.GetDOB();
            IsRegistredCandidate = this.db.Applications.OrderByDescending(x => x.RegDate).Any(
                 x =>
                 (x.MobileNo.Equals(applicant.Name) || x.Name.Equals(applicant.Name))
                 && DbFunctions.TruncateTime(x.DateOfBirth) == DbFunctions.TruncateTime(dob));


            if (IsRegistredCandidate)
            {
                candidateApplication =
                    this.db.Applications.OrderByDescending(x => x.RegDate).FirstOrDefault(
                        x =>
                        (x.MobileNo.Equals(applicant.Name) || x.Name.Equals(applicant.Name))
                        && DbFunctions.TruncateTime(x.DateOfBirth) == DbFunctions.TruncateTime(dob));
            }

            //if candidate is registred in system
            if (IsRegistredCandidate && candidateApplication != null)
            {

                   //
                    return RedirectToAction(nameof(LetterAsync), new { Id = candidateApplication.Id });


                    //this.ViewBag.Message = "झारखण्ड राज्य का काउंसलिंग लेटर संभवतः 15 जुलाई से डाउनलोड होगी, कृपया दिए गए दिनांक से काउंसलिंग लेटर डाउनलोड करने का प्रयास करें | अधिक जानकारी के लिए वेबसाइट देखते रहें या हेल्पलाइन नंबर पे कॉल करें | ";
     

            }
            else
            {
                this.ViewBag.Message = "OPPS! We dont found any registration details with your name 'or' mobile no. " + applicant.Name;
            }
            UI:
            LoadUI();
            return View(applicant);

        }

        public async Task<ActionResult> LetterAsync(int Id=0)
        {
            if (Id == 0)
            {
                return RedirectToAction(nameof(CounsellingLetter));
            }
            if (db.Applications.Any(x => x.Id == Id))
            {
                var Application = db.Applications.FirstOrDefault(x => x.Id == Id);
                ApplicationViewModel application = new ApplicationViewModel();
                application.Id = Application.Id;
                application.MobileNo = Application.MobileNo;
                application.Name = Application.Name;
                application.SelectedCourse = Application.Courses;
                application.DateOfBirth = Application.DateOfBirth;
                application.CounsellingDate = Application.CounsellingDate;

                MemoryStream stream = new MemoryStream();
                byte[] byteInfo = await PrepareCounsellingLetter(application);
                stream.Write(byteInfo, 0, byteInfo.Length);
                stream.Position = 0;
                return new FileStreamResult(stream, "application/pdf");
            }
            else
            {
               return RedirectToAction(nameof(CounsellingLetter));
            }

           
        }

        private void LoadUI()
        {
            ViewBag.DD = new SelectList(DateOfBirthHelper.DD(), "Value", "Text");
            ViewBag.MM = new SelectList(DateOfBirthHelper.MM(), "Value", "Text");
            ViewBag.YYYY = new SelectList(DateOfBirthHelper.YYYY(), "Value", "Text");
        }

        private async Task<byte[]> PrepareCounsellingLetter(ApplicationViewModel applicant)
        {



            //if candidate is registred in system
            if (applicant != null)
            {
                CounsellingLetterHelper clHelper = new CounsellingLetterHelper();
 
                //}



                #region CounsellingLetter

                MemoryStream stream = new MemoryStream();
                byte[] byteInfo =
                    CounsellingLetterHelper.getCounselling(
                        applicant.Name,
                        applicant.DateOfBirth,
                        applicant.Id,
                        applicant.MobileNo,
                        applicant.SelectedCourse,
                        applicant.CounsellingDate.Value
                        ).ToArray();// await this.CList()).ToArray();



                return byteInfo;


                #endregion
            }
            return null;
        }


    }
}