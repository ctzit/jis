﻿using JIS.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JIS.Web.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ForCounselling()
        {
            LoadUI();
            return View();
        }

        [HttpPost]
        public ActionResult ForCounselling(ApplicationModel application)
        {
            if (application == null) throw new ArgumentNullException(nameof(application));

            string Message = string.Empty;
            if (ModelState.IsValid)
            {
                if (RegistrationRepository.SaveRegistration(application, out Message))
                {
                    return RedirectToAction("LetterAsync", "Download", new { id = application.Id });
                }
                else
                {
                    ViewBag.ErrorMessage = Message;
                }
            }
            
            LoadUI();
            return View(application);

        }

        private void LoadUI()
        {
            ViewBag.CounsellingDate = new SelectList(DateOfBirthHelper.CounsellingDate(), "Value", "Text");
            ViewBag.Courses = new SelectList(DateOfBirthHelper.Courses(), "Value", "Text");
        }

    }
}