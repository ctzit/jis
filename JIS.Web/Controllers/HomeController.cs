﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JIS.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<string> FileNames = new List<string>();
            DirectoryInfo directoryInfo = new DirectoryInfo(Server.MapPath("~/img"));
            FileNames.AddRange(directoryInfo.GetFiles("*.jpg", SearchOption.AllDirectories).Select(x=>x.Name).ToList());
            return View(FileNames);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}